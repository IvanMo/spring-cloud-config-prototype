package client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringCloudConfigClientPrototypeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudConfigClientPrototypeApplication.class, args);
    }
}
// gradlew bootRun --args='--spring.profiles.active=...'
@RefreshScope
@RestController
class MessageRestController {

    @Value("${environment:default}")
    private String environment;

    @Value("${version:default}")
    private String version;

    @RequestMapping("/config")
    String getConfig() {
        return "environment: " + this.environment + "; version: " + this.version;
    }
}
